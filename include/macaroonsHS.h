/* Auxiliary C code for Ghttp.
 *
 * Copyright (c) 1999 Manuel M. T. Chakravarty
 *
 * This is required due to the inability of GHC's FFI to pass structures from C
 * to Haskell.
 */

#ifndef __MACAROONSHS_H__
#define __MACAROONSHS_H__


#include <macaroons.h>

typedef enum macaroon_returncode ReturnCode;

/* returns a reference to a newly allocated memory area holding the result of 
 * the corresponding vanilla `libghttp' function
 */
//ghttp_current_status *ghttpHS_get_status (ghttp_request *a_request);


#endif /* __MACAROONSHS_H__ */
