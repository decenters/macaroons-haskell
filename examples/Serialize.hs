{-# LANGUAGE OverloadedStrings #-}

import Macaroons
import Data.Time
import Data.List
import System.IO
import Data.ByteString.Char8 (ByteString, pack, unpack, empty)

cta :: String -> IO Bool
cta caveat = 
  if "time >= " `isPrefixOf` caveat then
    case (parseTimeM True defaultTimeLocale "%Y-%m-%d" $ drop 7 caveat) of
      Just when -> do
        now <- getCurrentTime
        return $ (utctDay now) >= when
      Nothing -> do putStrLn ("parse fail:" ++ caveat) 
                    return False
  else return False

main :: IO ()
main = do 
         let m1 = check  $ create "loc" "secret" "name"
         let m2 = check  $ addFirstPartyCaveat m1 (after $ fromGregorian 2016 9 12)
         let sm2 = check $ serialize m2 MacaroonV1
         putStrLn ("Mac: " ++ (unpack sm2))
         let dm2 = check $ deserialize sm2
         v <- verifierCreate 
         satisfyGeneral v cta
         b <- verify v dm2 "secret" []
         if check b then
           putStrLn "Good!" 
         else 
           putStrLn $ "Bad!" 
  where after day = pack $ "time >= " ++ formatTime defaultTimeLocale "%Y-%m-%d" day
        check v = case v of
                       Right v' -> v'
                       Left err -> error (show err)
