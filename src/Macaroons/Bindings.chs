{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE OverloadedStrings #-}

module Macaroons.Bindings
where
import Foreign.C.Types
import Foreign.C.String
import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Foreign.ForeignPtr
--import qualified Data.ByteString.Char8 as C
import Data.ByteString.Char8 (ByteString)
import Data.ByteString.Char8 as BS
import Data.ByteString.Unsafe as BS
import Data.Bool
import System.IO.Unsafe

{#context lib="libmacaroons" prefix="macaroons"#}

#include "macaroons.h"

--callback type
type VerifyCallback = Ptr CUChar -> CSize -> IO CInt
type VerifyLauncher = 
    Ptr () -> Ptr CUChar -> CSize -> IO CInt

foreign import ccall "wrapper"
  wrapCallback :: VerifyCallback -> IO (FunPtr VerifyCallback)

foreign import ccall "dynamic" unwrapCallback :: FunPtr VerifyCallback -> VerifyCallback 

foreign import ccall "wrapper"
  wrapLauncher :: VerifyLauncher -> IO (FunPtr VerifyLauncher)

toVerifyCallback :: (String -> IO Bool) -> IO (FunPtr VerifyCallback)
toVerifyCallback f =
  wrapCallback $ \p n -> 
  do s <- peekCStringLen (castPtr p, fromIntegral n);
     b <- f s
     return $ fromBool (not b)

callbackLauncher :: IO (FunPtr VerifyLauncher)
callbackLauncher =
  wrapLauncher $ \cb pred pred_sz ->
  let f = unwrapCallback $ castPtrToFunPtr cb in
    f pred pred_sz

-- Strings with explicit length
--
allocOutStrLen:: ((Ptr (Ptr CUChar), Ptr CSize) -> IO c) -> IO c
allocOutStrLen f = alloca $ \a -> alloca $ \b -> f (a, b)

withByteString ::ByteString -> (Ptr CChar -> IO a) -> IO a
withByteString s f = BS.useAsCString s $ \str ->
  f (castPtr str)

withByteStringLen ::ByteString -> ((Ptr CUChar, CSize) -> IO a) -> IO a
withByteStringLen s f = BS.useAsCStringLen s $ \(str,len) ->
  f (castPtr str, fromIntegral $ len)

peekByteStringLen :: Ptr (Ptr CUChar) -> Ptr CSize -> IO ByteString
peekByteStringLen ps pn = do s <- peek ps
                             n <- peek pn
                             BS.packCStringLen (castPtr s, fromIntegral n) 

withArrayLenIntConv :: (Storable a, Num n) => [a] -> ((Ptr a, n) -> IO b) -> IO b
withArrayLenIntConv s f = withArrayLen s $ (\n p -> f (p, fromIntegral n))

-- |Obtain Haskell 'Bool' from C return value.
-- Zero corresponds to no error, thus True
cToNotBool :: CInt -> Bool
cToNotBool  = not . toBool


data MacaroonStruct 
{#pointer *macaroon as MacaroonPtr -> MacaroonStruct #}
data VerifierStruct
{#pointer *macaroon_verifier as VerifierPtr -> VerifierStruct #}

{#typedef size_t CSize#}

{#enum macaroon_returncode as ReturnCode {underscoreToCase}#}
instance Show ReturnCode where
  show MacaroonSuccess        = "MacaroonSuccess" 
  show MacaroonOutOfMemory    = "MacaroonOutOfMemory"
  show MacaroonHashFailed     = "MacaroonHashFailed"
  show MacaroonInvalid        = "MacaroonInvalid"
  show MacaroonTooManyCaveats = "MacaroonTooManyCaveats"
  show MacaroonCycle          = "MacaroonCycle"
  show MacaroonBufTooSmall    = "MacaroonBufTooSmall"
  show MacaroonNotAuthorized  = "MacaroonNotAuthorized"
  show MacaroonNoJsonSupport  = "MacaroonNoJsonSupport"

{#enum macaroon_format as MacaroonFormat {underscoreToCase}#}

instance Eq ReturnCode where
  (==) c1 c2 = (fromEnum c1) == (fromEnum c2)

{#fun pure macaroon_create as ^ 
   {withByteStringLen* `ByteString'&, withByteStringLen* `ByteString'&, withByteStringLen* `ByteString'&, alloca- `CInt' peek*} 
   -> `MacaroonPtr' #}

foreign import ccall "macaroon.h &macaroon_destroy"
  macaroonDestroyPtr :: FunPtr (MacaroonPtr -> IO ())

{#fun pure macaroon_copy as ^ {id `MacaroonPtr', alloca- `CInt' peek*} -> `MacaroonPtr' #}

{#fun pure macaroon_validate as ^ {id `MacaroonPtr'} -> `Bool' cToNotBool #}
{#fun pure macaroon_add_first_party_caveat as ^ 
   {id `MacaroonPtr', withByteStringLen* `ByteString'&, alloca- `CInt' peek*} 
   -> `MacaroonPtr' #}

{#fun pure macaroon_num_third_party_caveats as ^ {id `MacaroonPtr'} -> `CInt' #}
{#fun pure macaroon_third_party_caveat as ^ {id `MacaroonPtr', `Int', allocOutStrLen- `ByteString'& peekByteStringLen*, allocOutStrLen- `ByteString'& peekByteStringLen*} -> `CInt' #}
{#fun pure macaroon_prepare_for_request as ^ {id `MacaroonPtr', id `MacaroonPtr', alloca- `CInt' peek*} -> `MacaroonPtr' #}

{#fun pure macaroon_location as ^ {id `MacaroonPtr', allocOutStrLen- `ByteString'& peekByteStringLen*} -> `()' #}
{#fun pure macaroon_identifier as ^ {id `MacaroonPtr', allocOutStrLen- `ByteString'& peekByteStringLen*} -> `()' #}
{#fun pure macaroon_signature as ^ {id `MacaroonPtr', allocOutStrLen- `ByteString'& peekByteStringLen*} -> `()' #}

{#fun pure macaroon_serialize_size_hint as ^ {id `MacaroonPtr', `MacaroonFormat'} -> `CSize' #}
{#fun pure macaroon_serialize as ^ {id `MacaroonPtr', `MacaroonFormat', id `Ptr CUChar', `CSize', alloca- `CInt' peek*} -> `CSize' #}
{#fun pure macaroon_deserialize as ^ {withByteStringLen* `ByteString'&, alloca- `CInt' peek*} -> `MacaroonPtr' #}
{#fun pure macaroon_inspect_size_hint as ^ {id `MacaroonPtr'} -> `CSize' #}
{#fun pure macaroon_inspect as ^ {id `MacaroonPtr', id `Ptr CChar', `CSize', alloca- `CInt' peek*} -> `CInt' #}
{#fun pure macaroon_cmp as ^ {id `MacaroonPtr', id `MacaroonPtr'} -> `CInt' #}

{-- NB: adding third party caveats is impure b/c of random nonces for encryption -}
{#fun macaroon_add_third_party_caveat as ^ 
   {id `MacaroonPtr', withByteStringLen* `ByteString'&, withByteStringLen* `ByteString'&, 
    withByteStringLen* `ByteString'&,  alloca- `CInt' peek*} 
   -> `MacaroonPtr' #}

{-- NB: verifier functions are impure -}
{#fun macaroon_verifier_create as ^ {} -> `VerifierPtr' #}

foreign import ccall "macaroon.h &macaroon_verifier_destroy"
  macaroonVerifierDestroyPtr :: FunPtr (VerifierPtr -> IO ())

{#fun macaroon_verifier_destroy as ^ {id `VerifierPtr'} -> `()' #}

{#fun macaroon_verifier_satisfy_exact as ^ 
  {id `VerifierPtr', withByteStringLen* `ByteString'&, alloca- `CInt' peek*} -> `CInt' #}
{#fun macaroon_verifier_satisfy_general as ^
  {id `VerifierPtr', id `FunPtr VerifyLauncher', `Ptr ()', alloca- `CInt' peek*} -> `CInt' #}

{#fun macaroon_verify as ^ 
  {id `VerifierPtr', id `MacaroonPtr', withByteStringLen* `ByteString'&, 
   withArrayLenIntConv* `[MacaroonPtr]'&, alloca- `CInt' peek*} -> `CInt' #}
