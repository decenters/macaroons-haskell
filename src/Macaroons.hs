{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Macaroons
       ( Macaroon
       , Verifier
       , ReturnCode(..)
       , MacaroonFormat(..)
       , validate
       , location
       , identifier
       , signature
       , copy
       , serialize
       , inspect
       , create
       , deserialize
       , thirdPartyCaveats
       , prepareForRequest
       , addFirstPartyCaveat
       , addThirdPartyCaveat
       , verifierCreate
       , verify
       , satisfyExact
       , satisfyGeneral
       , suggestedSecretLength
       )
where

import Macaroons.Bindings  
import Foreign.Marshal.Alloc
import Foreign.C.String
import Foreign.C.Types
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal.Utils
import Debug.Trace
import Numeric
import Data.Char
import Data.ByteString.Char8 (ByteString, pack, empty)
import Data.ByteString.Base16 (encode)
import Data.ByteString.UTF8 (fromString, toString)
import System.IO.Unsafe

type Macaroon = ForeignPtr MacaroonStruct       
type Verifier = ForeignPtr VerifierStruct       

suggestedSecretLength :: Int
suggestedSecretLength = 32

validate :: Macaroon -> Bool
validate m = unsafePerformIO $
  withForeignPtr m $ \x ->
  return $ macaroonValidate x

location :: Macaroon -> ByteString
location m = unsafePerformIO $
  withForeignPtr m $ \x ->
  return $ macaroonLocation x

identifier :: Macaroon -> ByteString
identifier m = unsafePerformIO $
  withForeignPtr m $ \x ->
  return $ macaroonIdentifier x

signature :: Macaroon -> ByteString
signature m = unsafePerformIO $
  withForeignPtr m $ \x ->
  return $ encode $ macaroonSignature x

copy :: Macaroon -> Either ReturnCode Macaroon
copy m1 = unsafePerformIO $
  withForeignPtr m1 $ \m1' ->
  newMacaroon $ macaroonCopy m1' 
          
serialize :: Macaroon -> MacaroonFormat -> Either ReturnCode ByteString
serialize m fmt = unsafePerformIO $
  withForeignPtr m $ \m' ->
  let sz = macaroonSerializeSizeHint m' fmt in
  allocaBytes (fromIntegral sz) $ \charPtr -> 
  let (rv, err) = macaroonSerialize m' fmt charPtr sz in
    if rv < 0 then do
      return $ Left (toEnum (fromIntegral err))
    else do
      -- XXX: is this right?
      str <- peekCStringLen (castPtr charPtr, fromIntegral rv)
      return $ Right (fromString . toString $ pack str)
  
inspect :: Macaroon -> Either ReturnCode String
inspect m = unsafePerformIO $
  withForeignPtr m $ \m' ->
  let sz = macaroonInspectSizeHint m' in
  allocaBytes (fromIntegral sz) $ \charPtr -> 
  let (rv, err) = macaroonInspect m' charPtr sz in
    if rv < 0 then do
      return $ Left (toEnum (fromIntegral err))
    else do
      str <- peekCString charPtr
      return $ Right str

-- XXX: unecessary? ForeignPtr has an Eq instance
--instance Eq Macaroon where
--  m1 == m2 = unsafePerformIO $
--             withForeignPtr m1 $ \m1' ->
--             withForeignPtr m2 $ \m2' ->
--             return $ fromIntegral (macaroonCmp m1' m2') == 0 
  
create :: ByteString -> ByteString -> ByteString -> Either ReturnCode Macaroon
create loc key keyId = unsafePerformIO $
  newMacaroon $ macaroonCreate loc key keyId 

deserialize :: ByteString -> Either ReturnCode Macaroon
deserialize s = unsafePerformIO $
  newMacaroon $ macaroonDeserialize s

thirdPartyCaveats :: Macaroon -> Either ReturnCode [(ByteString, ByteString)]
thirdPartyCaveats m = unsafePerformIO $
  withForeignPtr m $ \m' ->
    let num = fromIntegral $ macaroonNumThirdPartyCaveats m' in
    let cavs = sequence $
               map (\i ->
                     let (err, loc, id) = macaroonThirdPartyCaveat m' i in
                       if fromIntegral err < 0 then
                         Nothing
                       else
                         Just (loc, id))
                   [0..num-1]
    in case cavs of
        Just cs -> return $ Right cs
        Nothing -> return $ Left MacaroonInvalid

prepareForRequest :: Macaroon -> Macaroon -> Either ReturnCode Macaroon
prepareForRequest m d = unsafePerformIO $
  withForeignPtr m $ \m' ->
  withForeignPtr d $ \d' ->
  newMacaroon $ macaroonPrepareForRequest m' d' 

addFirstPartyCaveat :: Macaroon -> ByteString -> Either ReturnCode Macaroon
addFirstPartyCaveat m c = unsafePerformIO $
  withForeignPtr m $ \m' ->
  newMacaroon $ macaroonAddFirstPartyCaveat m' c

addThirdPartyCaveat :: Macaroon -> ByteString -> ByteString -> ByteString -> IO (Either ReturnCode Macaroon)
addThirdPartyCaveat m loc key keyId = unsafePerformIO $
  withForeignPtr m $ \m' ->
  newMacaroon <$> macaroonAddThirdPartyCaveat m' loc key keyId

verifierCreate :: IO Verifier
verifierCreate = macaroonVerifierCreate >>= newVerifier 

verify :: Verifier -> Macaroon -> ByteString -> [Macaroon] -> IO (Either ReturnCode Bool)
verify v m secret dms = 
  withForeignPtr v $ \v' -> 
  withForeignPtr m $ \m' ->
  withMany withForeignPtr dms $ \dms' -> do
  (rv, err) <- macaroonVerify v' m' secret dms'
  if rv == 0 then
    return $ Right True
  else if (toEnum (fromIntegral err)) == MacaroonNotAuthorized then
    return $ Right False
  else
    return $ Left (toEnum (fromIntegral err))

satisfyExact :: Verifier -> ByteString -> IO (Either ReturnCode Bool)
satisfyExact v pred = 
  withForeignPtr v $ \v' -> do
  (rv, err) <- macaroonVerifierSatisfyExact v' pred
  if rv < 0 then do
    return $ Left (toEnum (fromIntegral err))
  else
    return $ Right True

satisfyGeneral :: Verifier -> (String -> IO Bool) -> IO (Either ReturnCode Bool)
satisfyGeneral v f = 
  withForeignPtr v $ \v' ->
  do callback <- toVerifyCallback f
     launch <- callbackLauncher
     (rv, err) <- macaroonVerifierSatisfyGeneral v' launch (castFunPtrToPtr callback)
     if rv < 0 then do
       return $ Left (toEnum (fromIntegral err))
     else
       return $ Right True

-- not exported --
newMacaroon :: (MacaroonPtr, CInt) -> IO (Either ReturnCode Macaroon)
newMacaroon (m', err) = 
  if m' == nullPtr then do
    return $ Left (toEnum (fromIntegral err))
  else do
    m <- newForeignPtr macaroonDestroyPtr m'
    return $ Right m

-- not exported --
newVerifier :: VerifierPtr -> IO Verifier
newVerifier m' = 
  if m' == nullPtr then do
    fail "unable to create verifier"
  else do
    m <- newForeignPtr macaroonVerifierDestroyPtr m'
    return m
